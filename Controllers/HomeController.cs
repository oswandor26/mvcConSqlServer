﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
// importamos el modelo 
using Users.Models; 
// libreria para las configuracion conectionString 
using System.Configuration;
// importamos las libreri de sql sever
using System.Data.SqlClient;
using System.Data; 

namespace Users.Controllers
{
    public class HomeController : Controller
    {

        // declaramos otro ConectrionString para aguardar desde el constructor
        public String ConectionString;

        // hacemos una lista para alacemanr todos los usuarios  
        private static List<Usurios> listadeUsers;


        public HomeController(IConfiguration configuration)
        {  
            // obtenemos el connectionstring de myDb1 en appsetting.json 
            ConectionString = configuration.GetConnectionString("myDb2");
        

        }
        //GET: /Home/Index
        public IActionResult Index()
        {
            try {


                SqlConnection con = new SqlConnection(ConectionString);

                con.Open();
                Console.WriteLine("Open server");

                //ejecutamos un Query para selecionar todos los campos 
                SqlCommand comma = new SqlCommand("SELECT * FROM usertabledb", con);
                comma.CommandType = CommandType.Text;

                //creamos un Reader 
                SqlDataReader reader =  comma.ExecuteReader();


                //importante hacer la lista 

                listadeUsers = new List<Usurios>();

                //mientras exita reader almacenar en un nuevo objeto para agregarlos en una listaUsers 
                while (reader.Read()) {

                    Users.Models.Usurios nuevoUsurio = new Usurios();

                    nuevoUsurio.idUser = Convert.ToInt32( reader["iduser"]);
                    nuevoUsurio.nombre = reader["nombre"].ToString();
                    nuevoUsurio.apellido = reader["apellido"].ToString();
                    nuevoUsurio.ano = reader["ano"].ToString();
                    nuevoUsurio.telefono = reader["telefono"].ToString();


                    Console.WriteLine(reader["nombre"].ToString()); 

                    listadeUsers.Add(nuevoUsurio); 
                
                }

              
            }
            catch (System.Exception  mensaje){
                Console.WriteLine(mensaje); 
            }
            //retornamos una lista de USers 
            return View(listadeUsers);
         }

        //GET: /Home/createusers
        public IActionResult createusers()
        {
        
            return View(); 
        }



        [HttpPost]
        public IActionResult createusers(Usurios objeusers) {


            try
            {   //creamos la conecion al servidor
                SqlConnection con = new SqlConnection(ConectionString);

                con.Open();
                
                SqlCommand coma = new SqlCommand() ;

                coma.Connection = con;
                //ingremos los datos del objetoUsers  
                coma.CommandText = String.Format("INSERT INTO usertabledb (nombre , apellido ,ano ,telefono  ) VALUES('{0}' , ' {1}' , '{2}' , '{3}')", objeusers.nombre,objeusers.apellido , objeusers.ano , objeusers.telefono);

                coma.ExecuteNonQuery();

                con.Close(); 

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return View(); 
        }


        [HttpGet] 
        //  Get: /Home/Edit?iduser=1 
        public IActionResult Edit(int? iduser){
            if (iduser == null) {

                return RedirectToAction("Index", "Home"); 
            }

            Usurios? ousuario = listadeUsers.Where(c => c.idUser == iduser).FirstOrDefault();

            return View(ousuario);  
        }



        // POST: /Home/Edit 
        [HttpPost]
        public IActionResult Edit(Usurios objeusers) {



            try
            {

                SqlConnection con = new SqlConnection(ConectionString);
                con.Open(); 


                SqlCommand comando = new SqlCommand();
                comando.Connection = con; 
                comando.CommandText = String.Format("UPDATE usertabledb SET nombre='{1}'  , apellido='{2}' ,ano='{3}' , telefono='{4}'  WHERE iduser={0}", objeusers.idUser.ToString() , objeusers.nombre, objeusers.apellido, objeusers.ano ,objeusers.telefono);
                comando.ExecuteNonQuery();

                con.Close(); 



            }
            catch(Exception e)
            {
                Console.WriteLine(e); 

            }



            return View(); 
        }








        [HttpGet]
        public IActionResult delete(int? iduser)
        {
            try
            {

                SqlConnection con =  new SqlConnection(ConectionString);
                con.Open(); 

                SqlCommand coman = new SqlCommand();

                coman.Connection = con;
                coman.CommandText = String.Format("DELETE FROM usertabledb WHERE iduser ={0}", iduser.ToString());
                coman.ExecuteNonQuery();


                con.Close(); 

                return RedirectToAction("Index", "Home");

            }
            catch(Exception) {
            
            }




            return RedirectToAction("Index" , "Home"); 
        }


        public IActionResult Privacy()
        {
            return View();
        }

    }
}